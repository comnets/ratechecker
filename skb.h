#ifndef _NCK_PACKET_H_
#define _NCK_PACKET_H_

#ifdef __cplusplus
#include <cstdint>
#include <cstdio>
#else
#include <stdint.h>
#include <stdio.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

struct sk_buff {
	size_t len;
	uint8_t *data;
	uint8_t *head;
};

void skb_new(struct sk_buff *skb, uint8_t *buffer, size_t len);
void skb_load(struct sk_buff *skb, uint8_t *buffer, size_t len);

size_t skb_headroom(struct sk_buff *skb);
uint8_t *skb_push(struct sk_buff *skb, size_t len);
void skb_push_u8(struct sk_buff *skb, uint8_t value);
void skb_push_u16(struct sk_buff *skb, uint16_t value);
void skb_push_u32(struct sk_buff *skb, uint32_t value);

int pskb_may_pull(struct sk_buff *skb, size_t len);
uint8_t *skb_pull(struct sk_buff *skb, size_t len);
uint8_t skb_pull_u8(struct sk_buff *skb);
uint16_t skb_pull_u16(struct sk_buff *skb);
uint32_t skb_pull_u32(struct sk_buff *skb);

void skb_print(FILE *file, struct sk_buff *skb);
void skb_print_part(FILE *file, struct sk_buff *skb, size_t bytes, size_t bytes_per_block, size_t blocks_per_line);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _NCK_PACKET_H_ */

